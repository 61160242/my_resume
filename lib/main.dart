import 'package:flutter/material.dart';

void main() {
  runApp(MyResume());
}

class MyResume extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget nameSection = Container(
      padding: const EdgeInsets.all(25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            child: Text(
              'KANTAPOND AONRAKSA',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
          ),
        ],
      ),
    );

    Widget titleSection = Container(
      
      // padding: const EdgeInsets.all(25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.all(15.0),
                  padding: const EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    color: Colors.indigo[200],
                    border: Border.all(width: 3, color: Colors.indigo),
                    borderRadius: BorderRadius.all(Radius.circular(
                            10.0) //                 <--- border radius here
                        ),
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    'ABOUT ME',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
                Container(
                  alignment: AlignmentDirectional.center,
                  child: Text(
                      'Miss Kantapond Aonraksa my nickname is Gun. I am 21 years old. my birthday July 17 2000.'),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
               
                Container(
                  margin: const EdgeInsets.all(15.0),
                  padding: const EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    color: Colors.indigo[200],
                    border: Border.all(width: 3, color: Colors.indigo),
                    borderRadius: BorderRadius.all(Radius.circular(
                            10.0) //                 <--- border radius here
                        ),
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    'EDUCATION',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
                
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'July 2018-present',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                      'Computer Science,Faculy of Informatics, Burapha University',
                      softWrap:true
                      ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    Widget title2Section = Container(
      margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.indigo[200],
        border: Border.all(width: 3, color: Colors.indigo),
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //                 <--- border radius here
            ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Text(
                    'SKILLS',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    Widget imgSection = Container(
      padding: EdgeInsets.all(25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'img/html-5.png',
            width: 60,
            height: 60,
            // fit: BoxFit.cover,
          ),
          Image.asset(
            'img/css.png',
            width: 60,
            height: 60,
            // fit: BoxFit.cover,
          ),
          Image.asset(
            'img/javascript.png',
            width: 60,
            height: 60,
            // fit: BoxFit.cover,
          ),
          Image.asset(
            'img/java.png',
            width: 60,
            height: 60,
            // fit: BoxFit.cover,
          ),
          Image.asset(
            'img/nodejs.png',
            width: 60,
            height: 60,
            // fit: BoxFit.cover,
          ),
        ],
      ),
    );
    return MaterialApp(
      title: 'Resume',
      home: Scaffold(
          // backgroundColor: Colors.indigo[100],
          appBar: AppBar(
            title: const Text(
              'My Resume',
            ),
            backgroundColor: Colors.indigo[200],
          ),
          body: ListView(
            padding: const EdgeInsets.all(25),
            children: [
              Image.asset(
                'img/profile.jpg',
                width: 200,
                height: 200,
                // fit: BoxFit.cover,
              ),
              nameSection,
              titleSection,
              title2Section,
              imgSection
            ],
          )),
    );
  }
}
